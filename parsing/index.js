function App(
    http = require('http'),
    fs = require('fs'),
    { 
        Transform,
        Stream 
    } = require('stream')
) {
    this._broken = '';
    this._cachedChunk = '';
    const data = new Stream.Readable({
        read(chunk) {
            chunk
        }
    });
    return http.createServer((req, res) => {
        req.on('data', chunk => {
            data.push(chunk);
        });
        req.on('end', async () => {
            data.pipe(new Transform({
                transform : (chunk, enc, done) => {
                    try {
                        let json = [];
                        let chunkToStr = Buffer.from(chunk).toString();
                        if (this._broken && chunkToStr != this._cachedChunk) {
                            chunkToStr = this._broken + chunkToStr;
                            this._broken = undefined;
                        }
                        this._broken = chunkToStr.split('\n').splice(-1, 1)[0];
                        if (this._broken) {
                            this._cachedChunk = chunkToStr + this._broken;
                        }
                        chunkToStr.split('\n').forEach((r) => {
                            doubleQuotes = r.split('"')[1];
                            r = r.split(',');
                            json.push(JSON.stringify({
                                item: r.shift(),
                                type: r.shift(),
                                [r.shift()]: r.shift(),
                                [r.shift()]: doubleQuotes || r.shift()
                            })+'\n');
                        });
                        done(null, Buffer.from(json.join('').toString(),'binary')); 
                    } catch (e) {
                        done(e,{});
                    }
                }
            })).pipe(fs.createWriteStream(`${Date.now()}_dump.json`));
            res.write('DONE');
            res.end();
        });
    });
};

try {
    App().listen(8080);
} catch (e) {
    console.log(`Err: ${e}`);
}
